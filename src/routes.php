<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    $stmt = $this->db->query('SELECT DATE(datum_cas) as datum, CAST (datum_cas as TIME) as cas,  
                            teplota, vlhkost FROM zaznam ORDER BY datum_cas desc LIMIT 5');
    $tplVars['zaznamy'] = $stmt->fetchAll();

    $stmt = $this->db->query('SELECT teplota, vlhkost FROM zaznam ORDER BY datum_cas DESC LIMIT 1 ');
    $tplVars['aktualni'] = $stmt->fetchAll();

    $stmt = $this->db->query('SELECT DATE(datum_cas) as datum, teplota, CAST (datum_cas as TIME) as cas 
                                FROM zaznam WHERE DATE(datum_cas) = DATE(now())-1 ORDER BY datum_cas DESC');
    $tplVars['prumerna'] = $stmt->fetchAll();

    $suma = 0;
    $pocet = 0;
    foreach ($tplVars['prumerna'] as $vars) {
        $suma += $vars['teplota'];
        $pocet++;
    }

    if ($pocet = 0) {
        $tplVars['prumer'] = $suma / $pocet;
    } else
    {
        $tplVars['prumer'] = NULL;
    }
    return $this->view->render($response, 'index.latte', $tplVars);
})->setName('index');



$app->get('/mereni', function (Request $request, Response $response, $args){
    $stmt = $this->db->query('SELECT DATE(datum_cas) as datum, CAST (datum_cas as TIME) as cas,  
                            teplota, vlhkost FROM zaznam ORDER BY datum_cas DESC ');
    $tplVars['zaznamy'] = $stmt->fetchAll();
    return $this->view->render($response, 'mereni.latte', $tplVars); 

})->setName('mereni');


$app->get('/api/nastaveni', function( Request $request, Response $response, $args){

        $stmt = $this->db->query("SELECT * FROM nastaveni");
        $nastaveni = $stmt->fetchAll();
        $response_data['nastaveni'] = $nastaveni;
        $response->write(json_encode($response_data));

        return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);

});


$app->post('/api', function (Request $request, Response $response, $args) {
    $params = $request->getParsedBody();
    //$params = $request->getQueryParams();

    $teplota = $params['teplota'];
    $vlhkost = $params['vlhkost'];
    $cas = $params['cas'];


    $stmt = $this->db->prepare("INSERT INTO zaznam
                    (datum_cas, teplota, vlhkost)
                    VALUES
                    (:cas, :teplota, :vlhkost)");


    $stmt->bindValue(':vlhkost', $vlhkost, PDO::PARAM_INT);
    $stmt->bindValue(':teplota', $teplota, PDO::PARAM_INT);
    $stmt->bindValue(':cas', $cas);

    $stmt->execute();
});





$app->get('/nastaveni', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    try {

        $stmt = $this->db->query('SELECT MAX(id_nastaveni) as id_nastaveni, frekvence, stav 
                                    FROM nastaveni GROUP BY id_nastaveni, frekvence, stav');
        $tplVars['nastaveni'] = $stmt->fetch();

    } catch(Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }

    return $this->view->render($response, 'nastaveni.latte', $tplVars);
})->setName('nastaveni');



$app->post('/nastaveni', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->
        prepare('UPDATE nastaveni SET
                    stav = :stav, frekvence = :frekvence
                    WHERE id_nastaveni = :id_nastaveni
                    ');

        if($data['stav'] == 'on') {
            $stav = TRUE;
        } else {
            $stav = FALSE;
        }

        $stmt->bindValue(':id_nastaveni', $data['id_nastaveni']);
        $stmt->bindValue(':stav', $stav, PDO::PARAM_BOOL);
        $stmt->bindValue(':frekvence', $data['customRange2']);

        $stmt->execute();

        //presmerovat
        return $response->withHeader('Location',
            $this->router->pathFor('index'));
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());

    }
});