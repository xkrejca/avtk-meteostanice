<?php
// source: create-meeting.latte

use Latte\Runtime as LR;

class Template61009484d6 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['location'])) trigger_error('Variable $location overwritten in foreach on line 27');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Tvorba schuzky<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <form action="<?php
		echo $router->pathFor("create-meeting");
?>" method="post">
        <label>Popis schuzky:</label>
        <input type="text" name="description" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['description']) /* line 11 */ ?>">
        <br>

        <label>Zacatek schuzky: (2019-12-19 16:39:57)</label>
        <input type="datetime-local" name="start" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['start']) /* line 16 */ ?>" required>
        <br>

        <label>Delka schuzky:</label>
        <input type="text" name="duration" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['duration']) /* line 21 */ ?>">
        <br>

        <label>Misto konani:</label>
        <select name="id_location" class="form-control" required>
            <option value="">Neznama adresa</option>
<?php
		$iterations = 0;
		foreach ($locations as $location) {
			?>                <option value="<?php echo LR\Filters::escapeHtmlAttr($location['id_location']) /* line 28 */ ?>">
                    <?php echo LR\Filters::escapeHtmlText($location['city']) /* line 29 */ ?>, <?php echo LR\Filters::escapeHtmlText($location['street_name']) /* line 29 */ ?>

                    <?php echo LR\Filters::escapeHtmlText($location['street_number']) /* line 30 */ ?>

                </option>
<?php
			$iterations++;
		}
?>
        </select>
        <br>

        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Vytvorit schuzku
        </button>
    </form>


<?php
	}

}
