<?php
// source: create-address.latte

use Latte\Runtime as LR;

class Template8c2973f226 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Pridani osoby s adresou<?php
	}


	function blockBody($_args)
	{
		extract($_args);
		?>    <form action="<?php
		echo $router->pathFor("create-address");
?>" method="post">
        <label>Mesto</label>
        <input type="text" name="city" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['city']) /* line 9 */ ?>">
        <br>
        <label>Nazev ulice</label>
        <input type="text" name="street_name" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['street_name']) /* line 13 */ ?>">
        <br>
        <label>Cislo popisne</label>
        <input type="number" name="street_number" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['street_number']) /* line 17 */ ?>">
        <br>
        <label>ZIP</label>
        <input type="text" name="zip" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['zip']) /* line 21 */ ?>">
        <br>
        <label>Zeme</label>
        <input type="text" name="country" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['country']) /* line 25 */ ?>">
        <br>
        <label>Popis</label>
        <input type="text" name="name" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['name']) /* line 29 */ ?>">

        <br>
        <label>Zemepisna sirka</label>
        <input type="text" name="latitude" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['latitude']) /* line 34 */ ?>">
        <br>
        <label>Zemepisna delka</label>
        <input type="text" name="longitude" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['longitude']) /* line 38 */ ?>">
        <br>

        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Pridat adresu
        </button>
    </form>
<?php
	}

}
