<?php
// source: persons.latte

use Latte\Runtime as LR;

class Template42b6cb6cb8 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>



<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Osoby<?php
	}


	function blockBody($_args)
	{
?>    <br>

    <div class="col-10">
    <table class="table table-striped w-auto">
           <tr>
               <th>Jmeno</th>
               <th>Prijmeni</th>
               <th>Prezdivka</th>
               <th>Pohlavi</th>
           </tr>
    </table>
    </div>
<?php
	}

}
