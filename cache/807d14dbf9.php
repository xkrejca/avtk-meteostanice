<?php
// source: edit.latte

use Latte\Runtime as LR;

class Template807d14dbf9 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['location'])) trigger_error('Variable $location overwritten in foreach on line 47');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Editace<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <?php
		if (isset($warning)) {
			?><p><?php echo LR\Filters::escapeHtmlText($warning) /* line 7 */ ?></p><?php
		}
?>


    <form action="<?php
		echo $router->pathFor("edit");
?>" method="post">
        <label for="first_name">Jmeno:</label>
        <input type="text" id="first_name" name="first_name" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['first_name']) /* line 12 */ ?>" required>
        <br>

        <label for="last_name">Prijmeni:</label>
        <input type="text" id="last_name" name="last_name" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['last_name']) /* line 17 */ ?>" required>
        <br>

        <label for="nickname">Prezdivka:</label>
        <input type="text" id="nickname" name="nickname" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['nickname']) /* line 22 */ ?>" required>
        <br>

        <label for="birth_day">Datum narozeni</label>
        <input type="date" id="birth_day" name="birth_day" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['birth_day']) /* line 27 */ ?>">
        <br>

        <label for="height">Vyska</label>
        <input type="number" id="height" min="30" max="250" step="1" name="height"
               class="form-control" value="<?php echo LR\Filters::escapeHtmlAttr($form['height']) /* line 32 */ ?>" required>
        <br>


        <label>Pohlavi:</label>
        <select name="gender" class="form-control">
            <option value="male">Muz</option>
            <option value="female">Zena</option>
            <option value="">Nezname</option>
        </select>
        <br>

        <label>Pridat adresu</label>
        <select name="id_location" class="form-control">
            <option value="">Neznama adresa</option>
<?php
		$iterations = 0;
		foreach ($locations as $location) {
			if ($form['id_location'] == $location['id_location']) {
				?>                    <option value="<?php echo LR\Filters::escapeHtmlAttr($location['id_location']) /* line 49 */ ?>" selected>
                        <?php echo LR\Filters::escapeHtmlText($location['city']) /* line 50 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($location['street_name']) /* line 50 */ ?>

                        <?php echo LR\Filters::escapeHtmlText($location['street_number']) /* line 51 */ ?>

                    </option>
<?php
			}
			else {
				?>                    <option value="<?php echo LR\Filters::escapeHtmlAttr($location['id_location']) /* line 54 */ ?>">
                        <?php echo LR\Filters::escapeHtmlText($location['city']) /* line 55 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($location['street_name']) /* line 55 */ ?>

                        <?php echo LR\Filters::escapeHtmlText($location['street_number']) /* line 56 */ ?>

                    </option>
<?php
			}
			$iterations++;
		}
?>
        </select>
        <br>
        <input type="hidden" value="<?php echo LR\Filters::escapeHtmlAttr($form['id_person']) /* line 62 */ ?>" name="id_per">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Potvrdit upravy
        </button>
    </form>

<?php
	}

}
