<?php
// source: meeting-profile.latte

use Latte\Runtime as LR;

class Templatedac6790fab extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['person'])) trigger_error('Variable $person overwritten in foreach on line 33');
		if (isset($this->params['prsn'])) trigger_error('Variable $prsn overwritten in foreach on line 55');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Profil schuzky<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <table class="table table-striped">
        <tr>
            <th>Zacatek schuzky</th>
            <th>Delka</th>
            <th>Popis</th>
            <th>Misto konani</th>
        </tr>
        <tr>
            <td><?php echo LR\Filters::escapeHtmlText($meeting['start']) /* line 15 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($meeting['duration']) /* line 16 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($meeting['description']) /* line 17 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($meeting['city']) /* line 18 */ ?>, <?php echo LR\Filters::escapeHtmlText($meeting['street_name']) /* line 18 */ ?>,
                <?php echo LR\Filters::escapeHtmlText($meeting['street_number']) /* line 19 */ ?>, <?php
		echo LR\Filters::escapeHtmlText($meeting['country']) /* line 19 */ ?></td>
            <td><a href="<?php
		echo $router->pathFor("edit-meeting");
		?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($meeting['id_meeting'])) /* line 20 */ ?>"
                   class="btn btn-primary">Upravit schuzku</a></td>
        </tr>
    </table>
    <br>

    <h3>Ucastnici</h3>
    <table class="table table-striped">
        <tr>
            <th>Jmeno</th>
            <th>Prijmeni</th>
            <th>Prezdivka</th>
        </tr>
<?php
		$iterations = 0;
		foreach ($persons as $person) {
?>
        <tr>
            <td><?php echo LR\Filters::escapeHtmlText($person['first_name']) /* line 35 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($person['last_name']) /* line 36 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($person['nickname']) /* line 37 */ ?></td>
            <td>
                <form action="<?php
			echo $router->pathFor("delete-from-meeting");
?>"
                      method="post" onsubmit="return confirm('Opravdu smazat?')">
                    <input type="hidden" name="id_person" value="<?php echo LR\Filters::escapeHtmlAttr($person['id_person']) /* line 41 */ ?>">
                    <input type="hidden" name="id_meeting" value="<?php echo LR\Filters::escapeHtmlAttr($meeting['id_meeting']) /* line 42 */ ?>">
                    <input type="submit" value="Smazat" class="btn btn-danger">
                </form>
            </td>
            <td><a href="<?php
			echo $router->pathFor("profile");
			?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($person['id_person'])) /* line 46 */ ?>" class="btn btn-primary">Profil</td>
        </tr>
<?php
			$iterations++;
		}
?>
    </table>
    <br>

    <h3>Pridat osobu</h3>
    <form action="<?php
		echo $router->pathFor("add-to-meeting");
?>" method="post">
    <select name="id_person" class="form-control">
<?php
		$iterations = 0;
		foreach ($prsns as $prsn) {
			?>                <option value="<?php echo LR\Filters::escapeHtmlAttr($prsn['id_person']) /* line 56 */ ?>">
                    <?php echo LR\Filters::escapeHtmlText($prsn['first_name']) /* line 57 */ ?> <?php echo LR\Filters::escapeHtmlText($prsn['last_name']) /* line 57 */ ?>

                    <?php echo LR\Filters::escapeHtmlText($prsn['nickname']) /* line 58 */ ?>

                </option>
<?php
			$iterations++;
		}
?>
    </select>
        <input type="hidden" name="id_meeting" value="<?php echo LR\Filters::escapeHtmlAttr($meeting['id_meeting']) /* line 62 */ ?>">
        <br>
        <input type="submit" value="Pridat" class="btn btn-primary">
    </form>
<?php
	}

}
