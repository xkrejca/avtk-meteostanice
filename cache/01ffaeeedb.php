<?php
// source: index.latte

use Latte\Runtime as LR;

class Template01ffaeeedb extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Index<?php
	}


	function blockBody($_args)
	{
?>    <div class="container">
        <table class="table table-striped w-auto" align="center">

            <tr align="center">
                <th colspan="4">Meteostanice</th>
            </tr>

            <tr align="center">
                <th colspan="2">Aktuální teplota</th>
                <th colspan="2">10°C</th>
            </tr>
            <tr align="center">
                <th colspan="2">Aktuální vlhkost</th>
                <th colspan="2">70%</th>
            </tr>
            <tr align="center">
                <th colspan="2">Průměrná denní teplota</th>
                <th colspan="2">7°C</th>
            </tr>
            <tr align="center">
                <th colspan="2">
                    <button type="submit" class="btn btn-warning">
                        <i class="fa fa-eye"></i>
                        Zmměnit frekvenci měření
                    </button>
                </th>
                <th colspan="2">
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-power-off"></i>
                        Vypnout měření
                    </button>
                </th>
            </tr>
                    <tr>
                        <td>Datum měření</td>
                        <td>Čas měření</td>
                        <td>Teplota</td>
                        <td>Vlhkost</td>
                    </tr>
                    <tr>
                        <td>04.04.2020</td>
                        <td>7:00</td>
                        <td>5°C</td>
                        <td>70%</td>
                    </tr>
                    <tr>
                        <td>04.04.2020</td>
                        <td>8:00</td>
                        <td>6°C</td>
                        <td>73%</td>
                    </tr>
                    <tr>
                        <td>04.04.2020</td>
                        <td>9:00</td>
                        <td>5°C</td>
                        <td>75%</td>
                    </tr>
                    <tr>
                        <td>04.04.2020</td>
                        <td>10:00</td>
                        <td>9°C</td>
                        <td>60%</td>
                    </tr>


            <tr align="center">
                <th colspan="4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-clock"></i>
                        Zobrazit všechna měření
                    </button>
                </th>
            </tr>
        </table>
    </div>
<?php
	}

}
