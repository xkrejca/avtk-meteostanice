<?php
// source: relations.latte

use Latte\Runtime as LR;

class Template0feae3ae55 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['relation'])) trigger_error('Variable $relation overwritten in foreach on line 13');
		if (isset($this->params['type'])) trigger_error('Variable $type overwritten in foreach on line 33');
		if (isset($this->params['prsn'])) trigger_error('Variable $prsn overwritten in foreach on line 42');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vztahy<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <h2>Vztahy</h2>
    <table class="table table-striped">
            <th>Jmeno</th>
            <th>Prijmeni</th>
            <th>Vztah</th>
        </tr>
<?php
		$iterations = 0;
		foreach ($relations as $relation) {
?>
        <tr>
            <td><?php echo LR\Filters::escapeHtmlText($relation['first_name']) /* line 15 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($relation['last_name']) /* line 16 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($relation['name']) /* line 17 */ ?></td>
            <td>
            <form action="<?php
			echo $router->pathFor("delete-relation");
?>"
                  method="post" onsubmit="return confirm('Opravdu smazat?')">
                <input type="hidden" name="id_relation" value="<?php echo LR\Filters::escapeHtmlAttr($relation['id_relation']) /* line 21 */ ?>">
                <input type="submit" value="Smazat" class="btn btn-danger">
            </form>
            </td>
        </tr>
<?php
			$iterations++;
		}
?>
    </table>
    <br>

    <form action="<?php
		echo $router->pathFor("relations");
?>" method="post">
        <p>Vyberte typ vztahu:</p>
        <select name="id_relation_type" class="form-control" required>
<?php
		$iterations = 0;
		foreach ($relation_types as $type) {
			?>                <option value="<?php echo LR\Filters::escapeHtmlAttr($type['id_relation_type']) /* line 34 */ ?>">
                    <?php echo LR\Filters::escapeHtmlText($type['name']) /* line 35 */ ?>

                </option>
<?php
			$iterations++;
		}
?>
        </select>
        <br>
        <p>Vyberte osobu:</p>
        <select name="id_person" class="form-control" required>
<?php
		$iterations = 0;
		foreach ($persons as $prsn) {
			?>                <option value="<?php echo LR\Filters::escapeHtmlAttr($prsn['id_person']) /* line 43 */ ?>">
                    <?php echo LR\Filters::escapeHtmlText($prsn['first_name']) /* line 44 */ ?> <?php echo LR\Filters::escapeHtmlText($prsn['last_name']) /* line 44 */ ?>

                    <?php echo LR\Filters::escapeHtmlText($prsn['nickname']) /* line 45 */ ?>

                </option>
<?php
			$iterations++;
		}
?>
        </select>
        <br>
        <p>Popis</p>
        <input type="text" name="description" class="form-control">
        <br>
        <input type="hidden" name="id_person1" value="<?php echo LR\Filters::escapeHtmlAttr($id) /* line 53 */ ?>">
        <input type="submit" value="Pridat vztah" class="btn btn-primary">
        <br>
    </form>

<?php
	}

}
