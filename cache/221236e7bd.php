<?php
// source: profile.latte

use Latte\Runtime as LR;

class Template221236e7bd extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Profil<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <table class="table table-striped">
        <tr>
            <th>Jmeno</th>
            <th>Prezdivka</th>
            <th>Pohlavi</th>
            <th>Vyska</th>
            <th>Datum narozeni</th>
            <th>Schuzky</th>
            <th>Vztahy</th>
            <th>Kontakty</th>
            <th>Adresa</th>
        </tr>
        <tr>
            <td><?php echo LR\Filters::escapeHtmlText($form['first_name']) /* line 20 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($form['nickname']) /* line 21 */ ?></td>
            <td class="gender"><?php echo LR\Filters::escapeHtmlText($form['gender']) /* line 22 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($form['height']) /* line 23 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($form['birth_day']) /* line 24 */ ?></td>
            <td><a href="<?php
		echo $router->pathFor("meetings");
		?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($form['id_person'])) /* line 25 */ ?>"
                   class="btn btn-primary">Zobrazit schuzky</td>

            <td><a href="<?php
		echo $router->pathFor("relations");
		?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($form['id_person'])) /* line 28 */ ?>"
                   class="btn btn-primary">Zobrazit vztahy</td>

            <td><a href="<?php
		echo $router->pathFor("contact");
		?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($form['id_person'])) /* line 31 */ ?>"
                   class="btn btn-primary">Zobrazit kontakty</td>

            <td><?php echo LR\Filters::escapeHtmlText($form['city']) /* line 34 */ ?>, <?php echo LR\Filters::escapeHtmlText($form['street_name']) /* line 34 */ ?>,
                <?php echo LR\Filters::escapeHtmlText($form['street_number']) /* line 35 */ ?>, <?php echo LR\Filters::escapeHtmlText($form['country']) /* line 35 */ ?></td>
        </tr>
        <tr>
            <td><a href="<?php
		echo $router->pathFor("edit");
		?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($form['id_person'])) /* line 38 */ ?>"
                   class="btn btn-primary">Upravit profil</a></td>

            <td>
                <form action="<?php
		echo $router->pathFor("delete");
?>" method="post" onsubmit="return confirm('Opravdu smazat?')">
                    <input type="hidden" name="id_person" value="<?php echo LR\Filters::escapeHtmlAttr($form['id_person']) /* line 43 */ ?>">
                    <input type="submit" value="Smazat" class="btn btn-danger">
                </form>
            </td>
        </tr>
    </table>
<?php
	}

}
