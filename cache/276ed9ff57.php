<?php
// source: edit-meeting.latte

use Latte\Runtime as LR;

class Template276ed9ff57 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['location'])) trigger_error('Variable $location overwritten in foreach on line 27');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Editace schuzky<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <?php
		if (isset($warning)) {
			?><p><?php echo LR\Filters::escapeHtmlText($warning) /* line 7 */ ?></p><?php
		}
?>

    <form action="<?php
		echo $router->pathFor("edit-meeting");
?>" method="post">
        <label for="description">Popis schuzky:</label>
        <input type="text" id="description" name="description" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($meeting['description']) /* line 11 */ ?>">
        <br>

        <label for="start">Zacatek schuzky: (2019-12-19 16:39:57)</label>
        <input type="datetime-local" id="start" name="start" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($meeting['start']) /* line 16 */ ?>" required>
        <br>

        <label for="duration">Delka schuzky:</label>
        <input type="text" id="duration" name="duration" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($meeting['duration']) /* line 21 */ ?>">
        <br>

        <label>Misto konani:</label>
        <select name="id_location" class="form-control" required>
            <option value="">Neznama adresa</option>
<?php
		$iterations = 0;
		foreach ($locations as $location) {
			if ($meeting['id_location'] == $location['id_location']) {
				?>                    <option value="<?php echo LR\Filters::escapeHtmlAttr($location['id_location']) /* line 29 */ ?>" selected>
                        <?php echo LR\Filters::escapeHtmlText($location['city']) /* line 30 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($location['street_name']) /* line 30 */ ?>

                        <?php echo LR\Filters::escapeHtmlText($location['street_number']) /* line 31 */ ?>

                    </option>
<?php
			}
			else {
				?>                    <option value="<?php echo LR\Filters::escapeHtmlAttr($location['id_location']) /* line 34 */ ?>">
                        <?php echo LR\Filters::escapeHtmlText($location['city']) /* line 35 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($location['street_name']) /* line 35 */ ?>

                        <?php echo LR\Filters::escapeHtmlText($location['street_number']) /* line 36 */ ?>

                    </option>
<?php
			}
			$iterations++;
		}
?>
        </select>
        <br>

        <input type="hidden" value="<?php echo LR\Filters::escapeHtmlAttr($meeting['id_meeting']) /* line 43 */ ?>" name="id_meeting">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Potvrdit upravy
        </button>
    </form>

<?php
	}

}
