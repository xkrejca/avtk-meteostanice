<?php
// source: create-person.latte

use Latte\Runtime as LR;

class Template8cf289b629 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Tvorba osoby<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <form action="<?php
		echo $router->pathFor("createPerson");
?>" method="post">
        <label>Jmeno:</label>
        <input type="text" name="firstName" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['firstName']) /* line 11 */ ?>" required>
        <br>

        <label>Prijmeni:</label>
        <input type="text" name="lastName" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['lastName']) /* line 16 */ ?>" required>
        <br>

        <label>Prezdivka:</label>
        <input type="text" name="nickName" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['nickName']) /* line 21 */ ?>" required>
        <br>

        <label>Datum narozeni</label>
        <input type="date" name="birthDate" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['birthDate']) /* line 26 */ ?>">
        <br>

        <label>Vyska</label>
        <input type="number" min="30" max="250" step="1" name="height"
               class="form-control" value="<?php echo LR\Filters::escapeHtmlAttr($form['height']) /* line 31 */ ?>" required>
        <br>

        <label>Pohlavi:</label>
        <select class="custom-select" name="gender">
            <option value="male">Muz</option>
            <option value="female">Zena</option>
            <option value="">Nezname</option>
        </select>
        <br>

        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Vytvorit osobu
        </button>
    </form>


<?php
	}

}
