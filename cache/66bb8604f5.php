<?php
// source: create-contact.latte

use Latte\Runtime as LR;

class Template66bb8604f5 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['contact'])) trigger_error('Variable $contact overwritten in foreach on line 16');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Tvorba kontaktu<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <form action="<?php
		echo $router->pathFor("create-contact");
?>" method="post">
        <label>Kontakt:</label>
        <input type="text" name="contact" class="form-control"
               value="<?php echo LR\Filters::escapeHtmlAttr($form['contact']) /* line 11 */ ?>" required>
        <br>

        <label>Typ kontaktu:</label>
        <select name="id_contact" class="form-control" required>
<?php
		$iterations = 0;
		foreach ($contacts as $contact) {
			?>                <option value="<?php echo LR\Filters::escapeHtmlAttr($contact['id_contact']) /* line 17 */ ?>">
                    <?php echo LR\Filters::escapeHtmlText($contact['name']) /* line 18 */ ?>

                </option>
<?php
			$iterations++;
		}
?>
        </select>
        <br>

        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Vytvorit kontakt
        </button>
    </form>


<?php
	}

}
