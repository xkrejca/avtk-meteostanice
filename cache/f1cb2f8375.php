<?php
// source: meetings.latte

use Latte\Runtime as LR;

class Templatef1cb2f8375 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['meeting'])) trigger_error('Variable $meeting overwritten in foreach on line 18');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Schuzky<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <?php
		if (isset($warning)) {
			?><h1><?php echo LR\Filters::escapeHtmlText($warning) /* line 8 */ ?></h1>
<?php
		}
		elseif (isset($meetings)) {
?>
    <h2>Schuzky</h2>
    <table class="table table-striped">
        <tr>
            <th>Zacatek</th>
            <th>Popis</th>
            <th>Delka</th>
            <th>Misto konani</th>
        </tr>
<?php
			$iterations = 0;
			foreach ($meetings as $meeting) {
?>
            <tr>
                <td><?php echo LR\Filters::escapeHtmlText($meeting['start']) /* line 20 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($meeting['description']) /* line 21 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($meeting['duration']) /* line 22 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($meeting['city']) /* line 23 */ ?>, <?php echo LR\Filters::escapeHtmlText($meeting['street_name']) /* line 23 */ ?>,
                    <?php echo LR\Filters::escapeHtmlText($meeting['street_number']) /* line 24 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($meeting['country']) /* line 24 */ ?></td>
                <td><a href="<?php
				echo $router->pathFor("meeting-profile");
				?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($meeting['id_meeting'])) /* line 25 */ ?>"
                       class="btn btn-primary">Profil schuzky</a></td>
                <td>
                    <form action="<?php
				echo $router->pathFor("delete-meeting");
?>"
                          method="post" onsubmit="return confirm('Opravdu smazat?')">
                        <input type="hidden" name="id_meeting" value="<?php echo LR\Filters::escapeHtmlAttr($meeting['id_meeting']) /* line 30 */ ?>">
                        <input type="submit" value="Smazat" class="btn btn-danger">
                    </form>
                </td>
            </tr>
<?php
				$iterations++;
			}
?>
    </table>
<?php
		}
		
	}

}
