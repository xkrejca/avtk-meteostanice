<?php
// source: contact.latte

use Latte\Runtime as LR;

class Templatebf21906b3e extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>
<br>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['contact'])) trigger_error('Variable $contact overwritten in foreach on line 14');
		if (isset($this->params['type'])) trigger_error('Variable $type overwritten in foreach on line 32');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Kontakty<?php
	}


	function blockBody($_args)
	{
		extract($_args);
		?>    <?php
		if (isset($warning)) {
			?><h1><?php echo LR\Filters::escapeHtmlText($warning) /* line 7 */ ?></h1>
<?php
		}
		elseif (isset($contacts)) {
?>
    <h2>Kontakt</h2>
    <table class="table table-striped">
        <th>Typ kontaktu</th>
        <th>Kontakt</th>
        </tr>
<?php
			$iterations = 0;
			foreach ($contacts as $contact) {
?>
            <tr>
                <td><?php echo LR\Filters::escapeHtmlText($contact['name']) /* line 16 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($contact['contact']) /* line 17 */ ?></td>
                <td>
                <form action="<?php
				echo $router->pathFor("delete-contact");
?>"
                      method="post" onsubmit="return confirm('Opravdu smazat?')">
                    <input type="hidden" name="id_contact" value="<?php echo LR\Filters::escapeHtmlAttr($contacts['id_contact']) /* line 21 */ ?>">
                    <input type="submit" value="Smazat kontakt" class="btn btn-danger">
                </form>
                </td>
            </tr>
<?php
				$iterations++;
			}
?>
    </table>
    <br>
    <form action="<?php
			echo $router->pathFor("contact");
?>" method="post">
        <p>Vyberte typ kontaktu:</p>
        <select name="id_contact_type" class="form-control">
<?php
			$iterations = 0;
			foreach ($contact_types as $type) {
				?>                <option value="<?php echo LR\Filters::escapeHtmlAttr($type['id_contact_type']) /* line 33 */ ?>">
                    <?php echo LR\Filters::escapeHtmlText($type['name']) /* line 34 */ ?>

                </option>
<?php
				$iterations++;
			}
?>
        </select>
        <br>
        <p>Kontakt:</p>
        <input type="text" name="contact" class="form-control">
        <input type="hidden" name="id_person" value="<?php echo LR\Filters::escapeHtmlAttr($id_person) /* line 41 */ ?>" class="form-control">
        <br>
        <input type="submit" value="Pridat kontakt" class="btn btn-primary">
    </form>
<?php
		}
		
	}

}
