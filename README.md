# Meteostanice #

* Meteostanice bude měřit teplotu a vlhkost vzduchu.
* Měření teploty podle mannheimských hodin, tj. v 7:00, ve 14:00 ve 21:00 kvůli výpočtu průměrné denní teploty.
* Databáze bude obsahovat všechny měření teploty a vlhkosti v čase.

## GUI ##

* Uživatel bude moci nastavit frekvenci měření nebo měření úplně vypnout.
* Uživatel bude moci zobrazit všechny data (měření), průměrné hodnoty ve dnech nebo aktuální den.